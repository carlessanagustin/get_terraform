#!/usr/bin/env bash
[[ -z ${TF_PATH} ]]      && export TF_PATH=/usr/local/bin
[[ -z ${TF_BINS} ]]      && export TF_BINS=$(pwd)


[[ -z ${JV_VERSION} ]]   && export JV_VERSION=jdk8.0.302
export JV_PLATFORM=macosx_x64
export JV_PKG=zulu8.56.0.21-ca-${JV_VERSION}-${JV_PLATFORM}
export JV_URL=https://cdn.azul.com/zulu/bin/${JV_PKG}.tar.gz

get_java(){
    wget ${JV_URL}
    tar zxvf ${JV_PKG}.tar.gz
    rm -f ${JV_PKG}.tar.gz
    
    export JAVA_HOME=$(pwd)/${JV_PKG}
    cat << EOF > ~/.my-java.sh
export JAVA_HOME=$(pwd)/${JV_PKG}
export PATH=$JAVA_HOME/bin:$PATH
EOF

    ./${JV_PKG}/bin/java -version
}

remove_java(){
    rm -Rf ./${JV_PKG}
}

list_java(){
    ls -la ./zulu*
}

"$@"
