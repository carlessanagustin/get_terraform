# (WIP) Get Terraform/Java bin versions

Download Terraform/Java binary version and place it into a `bin` folder for `PATH`.

Actions by the script:

* Download
* Rename
* Symlink

## Usage

* Get: `TF_VERSION=0.14.11 TF_PLATFORM=< linux_amd64 | darwin_amd64 > ./terraform.sh get`
* Remove: `TF_VERSION=0.12.0 ./terraform.sh remove`
* List: `./terraform.sh list`
