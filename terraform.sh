#!/usr/bin/env bash
[[ -z ${TF_VERSION} ]]   && export TF_VERSION=1.1.7 # 1.0.1 | 0.15.3 | 0.14.11
[[ -z ${TF_PLATFORM} ]]  && export TF_PLATFORM=darwin_amd64 # linux_amd64 | darwin_amd64
[[ -z ${TF_PATH} ]]      && export TF_PATH=/usr/local/bin
[[ -z ${TF_BINS} ]]      && export TF_BINS=$(pwd)

get(){
    wget https://releases.hashicorp.com/terraform/${TF_VERSION}/terraform_${TF_VERSION}_${TF_PLATFORM}.zip
    unzip terraform_${TF_VERSION}_${TF_PLATFORM}.zip
    rm -f terraform_${TF_VERSION}_${TF_PLATFORM}.zip
    sudo mv terraform ${TF_BINS}/terraform_${TF_VERSION}
    sudo ln -sf ${TF_BINS}/terraform_${TF_VERSION} ${TF_PATH}/terraform_${TF_VERSION}
    sudo ln -sf ${TF_BINS}/terraform_${TF_VERSION} ${TF_PATH}/terraform
}

remove(){
    sudo rm -f ${TF_PATH}/terraform_${TF_VERSION}
    rm -f ${TF_BINS}/terraform_${TF_VERSION}
}

list(){
    ls -la ${TF_PATH}/terraform*
}

"$@"
